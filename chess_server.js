"use strict";

var http = require("http"),
    url = require("url"),
    fs = require("fs"),
    path = require('path');


var port = 5002;
function handlerFunction(req, res) {
    // Send the HTTP header
    // HTTP Status: 200 : OK
    // Content Type: text/plain
    var url_parts = url.parse(req.url, true);
    var filePath = '.' + req.url;
if (filePath === './')
    filePath = './index.html';
    var extname = path.extname(filePath);
    var contentType = 'text/html';

    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            contentType = 'image/png';
            break;
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
    }

    switch(url_parts.pathname) {
        case "/":
            fs.readFile("log.json", "utf-8", (err, data)=>{
                if(err) throw err;
                fs.writeFile(JSON.parse(data).push([req,res]), "utf-8", ()=> {
                    console.log("logged request");
                });
            });
            fs.readFile("index.html", "utf-8", (err, data)=> {
                if(err) throw err;
                res.writeHead(200, { 'Content-Type': contentType });
                res.write(data);
                res.end();
            });
            break;
        case "/exo":
            fs.readFile("log.json", "utf-8", (err, data)=>{
                if(err) throw err;
                fs.writeFile(JSON.parse(data).push([req,res]), "utf-8", ()=> {
                    console.log("logged request");
                });
            });
            fs.readFile("exo.html", "utf-8", (err, data)=> {
                if(err) throw err;
                res.writeHead(200, { 'Content-Type': contentType });
                res.write(data);
                res.end();
            });
            break;
        case "/FEN_finder":
            fs.readFile("log.json", "utf-8", (err, data)=>{
                if(err) throw err;
                fs.writeFile(JSON.parse(data).push([req,res]), "utf-8", ()=> {
                    console.log("logged request");
                });
            });
            fs.readFile("FEN_finder.html", "utf-8", (err, data)=> {
                if(err) throw err;
                res.writeHead(200, { 'Content-Type': contentType });
                res.write(data);
                res.end();
            });
            break;
        default:
            fs.readFile("log.json", "utf-8", (err, data)=>{
                if(err) throw err;
                fs.writeFile(JSON.parse(data).push([req,res]), "utf-8", ()=> {
                    console.log("logged request");
                });
            });
            fs.readFile(filePath, function(error, content) {
        if (error) {
            if(error.code == 'ENOENT'){
                fs.readFile('./404.html', function(error, content) {
                    res.writeHead(200, { 'Content-Type': contentType });
                    res.end(content, 'utf-8');
                });
            }
            else {
                res.writeHead(500);
                res.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                res.end();
            }
        }
        else {
            res.writeHead(200, { 'Content-Type': contentType });
            res.end(content, 'utf-8');
        }
    });
        break;
    }
}

http.createServer(handlerFunction).listen(port);


// Console will print the message
console.log("Server running on port "+port);
